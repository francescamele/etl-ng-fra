import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArenaComponent } from './components/arena/arena.component';
import { Arena2Component } from './components/arena2/arena2.component';
import { ButtonsComponent } from './components/buttons/buttons.component';
import { CounterComponent } from './components/counter/counter.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [{
  component: ArenaComponent,
  path: 'arena'
}, {
  component: Arena2Component,
  path: 'arena2'
}, {
  component: ButtonsComponent,
  path: 'buttons'
}, {
  component: CounterComponent,
  path: 'counter'
}, {
  // rec II 08.02 loadchildren ??
  loadChildren: () => import('./modules/invoices/invoices.module').then(f => f.InvoicesModule),
  path: 'invoices'
}, {
  component: HomeComponent,
  path: ''
}, {
  component: NotFoundComponent,
  path: '**'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
