export class User {
    constructor(
        public readonly id: number,
        public name: string,
        public email: string
    ) {
        // 
    }

    // This property would be considered part of the response 
    // if you write <User[]> on the .get method
    // get readable(): string {
    //     return `User called ${this.name} with id ${this.id}`;
    // }
}