import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

// Interceptors:
import { TokenInterceptor } from './interceptors/token.interceptor';

// Routing module:
import { UsersRoutingModule } from './users-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './components/login/login.component';
import { RdfLoginComponent } from './components/rdf-login/rdf-login.component';
import { RowComponent } from './components/row/row.component';
import { TableComponent } from './components/table/table.component';
import { UserComponent } from './components/user/user.component';


@NgModule({
  declarations: [
    LoginComponent,
    RdfLoginComponent,
    RowComponent,
    TableComponent,
    UserComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    UsersRoutingModule
  ],
  providers: [
    {
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor
    }
  ]
})
export class UsersModule {
  constructor() {
    console.log('modulo creato');
  }
 }
