import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  // La Guard viene messa all'interno del root injector: possono usarla tutti:
  providedIn: 'root'
})
export class IsLoggedGuard implements CanActivate {

  constructor(private _router: Router) {
    // 
  }

  // canActivate, così come 2 e 3, prende due param formali
  canActivate(
    route: ActivatedRouteSnapshot,
    // ^un'istantanea del percorso di navigazione dove stiamo lavorando, 
    // eg anche query param del percorso, e quindi magari anche quello che 
    // l'utente vuole vedere: eg se non 
    // ActivatedRoute è il servizio da cui prendiamo param e queryParam, e 
    // questo snapshot allows us to get it in modo sincrono
    // Info solo sulla navigazione attuale
    state: RouterStateSnapshot
    // ^info su storia della navigazione. 23'??
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // ^Questa indentazione è per evitare che "return true" sia alla stessa 
    // altezza dei param formali
    // this._router.navigate(['/login']);
    // ^È asincrono: dato il segnale di redirect e poi faccio il return false: 
    // return false;
    // ^ci permette di accedere alla risorsa su cui stiamo lavorando

    return new Promise(resolve => {
      // Questo mi fa accedere alla pagina: 
      // return resolve(true);

      // alert('You are unauthorised');
      // Come ho scritto io:
      // setTimeout(() => resolve(this._router.navigate(['/login'])), 3000);

      // Soluzione di Andri senza setTimeout:
      // this._router.navigate(['/login']);
      // return resolve(false);

      // Come ha fatto Andri:
      setTimeout(() => {
        this._router.navigate(['/login']);
        return resolve(false);
      }, 1500);
    });
  }
  
}
