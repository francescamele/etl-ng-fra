import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components: 
import { LoginComponent } from './components/login/login.component';
import { RdfLoginComponent } from './components/rdf-login/rdf-login.component';
import { TableComponent } from './components/table/table.component';
import { UserComponent } from './components/user/user.component';
import { IsLoggedGuard } from './guards/is-logged.guard';

const routes: Routes = [{
  component: LoginComponent,
  path: 'login'
  }, {
    component: RdfLoginComponent,
    path: 'login-rdf'
  }, {
    canActivate: [IsLoggedGuard],
    children: [{
      // Se avessimo un comp profile con path: 'profile' lo metteremmo
      // qui, SOPRA ':id', perché sennò 'profile' potrebbe essere preso 
      // per un id:
      // sopra parte STATICA, sotto parte DINAMICA
    component: UserComponent,
    path: ':id'
    }],
    component: TableComponent,
    path: 'users'
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
