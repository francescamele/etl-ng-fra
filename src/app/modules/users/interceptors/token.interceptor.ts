import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { UserService } from '../services/user.service';

// Services:


@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private _userService: UserService) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const req = request.clone();

    if (this._userService.hasToken()) {
      req.headers.append('Authorization', `Bearer ${this._userService.getToken()}`);
    }

    // Passa al prossimo interceptor: next: gestisci la richiesta
    // req, non più request: è il param formale 57
    // Interceptor che becca il token e la ...?
    // lo fai thr interceptor, risparmiando tempo
    return next.handle(req);
  }
}
