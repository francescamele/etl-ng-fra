import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnDestroy, OnInit {

  // Come ho fatto inizialmente io: 
  // public user1: User = new User (
  //   1,
  //   'Aragorn',
  //   'kingofmen@arda.com'
  // )
  // public user2: User = new User (
  //   2,
  //   'Eowyn',
  //   'iamnoman@arda.com'
  // )
  // public user3: User = new User (
  //   3,
  //   'Gandalf',
  //   'istari@arda.com'
  // )
  // public user4: User = new User (
  //   4,
  //   'Frodo',
  //   'hobbit@arda.com'
  // )

  // public users = [
  //   this.user1,
  //   this.user2,
  //   this.user3,
  //   this.user4
  // ]

  // Come ha fatto Andri:
  // public users: User[] = [
  //   new User(1, 'Aragorn', 'kingofmen@arda.com'),
  //   new User(2, 'Eowyn', 'iamnoman@arda.com'),
  //   new User(3, 'Gandalf', 'istari@arda.com'),
  //   new User(4, 'Frodo', 'hobbit@arda.com')
  // ]

  // 28.01, per mettere l'array nel bottone ripristina
  private _usersInit: User[] = [];

  private _users$!: Subscription;

  // 28.01: commento questo per poter avere un arr vuoto
  // public users!: User[];
  public users: User[] = [];

  constructor(private _userService: UserService) { 
    // Quello che metto nel CORPO del costruttore viene 
    // eseguito per prima cosa: se chiamo qui la funzione, 
    // posso evitare di scrivere due volte l'inizializzazione 
    // di users[]. 
    // this.resetUsers();
  }

  public handleDeletion(user: User) {
    // let delUserPos = this.users.indexOf(user);
    // this.users.splice(delUserPos, 1);
    this.users.splice(
      this.users.indexOf(user),
      1
    );
  }

  public resetUsers(): void {
    this.users = this._usersInit;
  }

  public ngOnDestroy(): void {
    this._users$.unsubscribe();
  }

  public ngOnInit(): void {
    this._users$ = this._userService.list().subscribe(users => {
      // NON puoi scrivere this._usersInit = users perché così punteresti
      // allo stesso array: eliminando el in users, li elimineresti 
      // anche in _usersInit. 
      // Così invece stai creando un NUOVO array in cui ci butti tutti 
      // gli el presenti in users: 
      this._usersInit = [...users];
      this.users = users;

      // Cambio tutte le reference sull'ogg utente: 
      setTimeout(() => this.users.forEach(u => u.name = 'Pippo Franco'), 5000);
    });
  }
}
