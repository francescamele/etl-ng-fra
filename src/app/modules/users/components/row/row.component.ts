import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { User } from '../../models/user';

@Component({
  selector: '[app-row]',
  templateUrl: './row.component.html',
  styleUrls: ['./row.component.scss']
})
export class RowComponent implements OnChanges, OnInit {
  @Input() public user!: User;

  @Output() public delete: EventEmitter<User> = new EventEmitter;

  constructor() { 
    // setTimeout(() => this.user.name = 'Pippo Franco', 5000);
    // ^Questo puoi farlo anche su ciascun el dell'arr, per 
    // stimolarlo da fuori 
  }

  public handleDeleteClick(): void {
    this.delete.emit(this.user);
  }

  ngOnChanges(changes: SimpleChanges): void {
    // Non serve fare questo if perché c'è una sola propr:
    // if ('user' in changes)
    console.log(changes['user']);
  }

  ngOnInit(): void {
  }

}
