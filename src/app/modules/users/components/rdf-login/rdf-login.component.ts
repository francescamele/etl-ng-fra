import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { forbiddenValidator } from 'src/app/modules/shared/validators/string.validators';

@Component({
  selector: 'app-rdf-login',
  templateUrl: './rdf-login.component.html',
  styleUrls: ['./rdf-login.component.scss']
})
export class RdfLoginComponent implements OnInit {
  
  public form: FormGroup = new FormGroup({
    'email': new FormControl('', [
      Validators.required,
      Validators.email,
      forbiddenValidator('admin@eng.it')
      // Potrei anche scrivere la fn interna direttamente inline qui:
      // (c: AbstractControl) => {
        //   return {
          //     error: true
          //   }
          // }
          // Ma non lo facciamo perché la fn non richiamabile è stupida
    ]),
    'password': new FormControl('', [
      Validators.required,
      Validators.minLength(8),
      forbiddenValidator('password', 'Password', "PASSWORD")
    ]),
  });
  
  public isSubmitted: boolean = false;
      
  constructor() { }
  
  // public formEmail: AbstractControl | null = this.form.get('email');
  // public formPassword: AbstractControl | null = this.form.get('password');
  // ^Questo pure va bene, ma Andrea vuole la proprietà 
  // fittizia dei getter:
  public get formEmail(): FormControl {
    return this.form.get('email') as FormControl;
  }
  // ^Usi il FormControl perché SAI quello che gli stai passando. 
  // Fai type assertion con "as FormControl" per assicurargli che 
  // gli stai passando questo tipo di file. 

  public get formPassword(): FormControl {
    return this.form.get('password') as FormControl;
  }

  public handleSubmit(): void {
    this.isSubmitted = true;
    
    if (this.form.invalid) {
      return;
    }

    console.log(this.form.get('email')?.value);
  }

  ngOnInit(): void {
  }

}
