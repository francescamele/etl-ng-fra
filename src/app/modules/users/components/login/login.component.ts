import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public email: string = '';
  public password: string = '';

  public invalidEmails: string[] = [
    'admin@gmail.com',
    'captainkirk@enterprise.ufp',
    'notOk@gmail.com'
  ]

  public invalidPasswords: string[] = [
    'password',
    'Password'
  ]

  constructor() { }

  public handleSubmit(form: NgForm): void {
    if (form.invalid) {
      return;
    }
  }

  ngOnInit(): void {
  }

}
