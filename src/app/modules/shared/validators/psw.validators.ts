import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const pswForbiddenValidator = (...values: string[]): ValidatorFn => {
    return (c: AbstractControl): ValidationErrors | null => {
       for (const value of values) {
        if (value === c.value) {
            return {
                pswForbidden: true
            };
        }
       }
       
       return null;
    };
}