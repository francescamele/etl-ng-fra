import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

// Fn esterna
// Raccoglie dati utili ai fini della validazione
// - Restituisce la funzione di validazione interna, 

//   quella utilizzata di fatto pet validare il campo del form
export const forbiddenValidator = (...values: string[]): ValidatorFn => {
    // Quando viene invocata da Angular, le viene passato il Control 
    // istanziato sull'elemento del form
    return (c: AbstractControl): ValidationErrors | null => {
        for (const value of values) {
            // Per capire se c'è un err o no, dobbiamo confrontare tutti 
            // i valori passati alla funz forbiddenValidator con quelli 
            // scritti nel campo in input dall'utente

            // Se quanto digitato dall'utente (c.value) coincide con uno 
            // degli el dell'arr (una delle parole non ammesse), 
            // restituisco un errore
            if (value === c.value) {
                return {
                    // E' preferibile che la chiave da usare 
                    // nell'oggetto di errore abbia lo stesso nome 
                    // della funzione 
                    // 
                    // Questa chiave è l'elemento che viene cercato con 
                    // il metodo "hasError":
                    forbidden: true
                };
            }
        }

        // Nel caso in cui non si debba restituire un errore, 
        // Angular impone la restituzione del valore null 
        return null;
    };
};