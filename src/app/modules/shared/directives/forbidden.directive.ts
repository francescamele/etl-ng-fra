import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { forbiddenValidator } from '../validators/string.validators';

@Directive({
  // I providers ci servono per evitare di avere un'istanza sola per 
  // questa direttiva, mentre i casi d'uso sono tanti
  providers: [{
    multi: true,
    // In questo token ci sono tutte le REGOLE DI VALIDAZIONE: 
    provide: NG_VALIDATORS,
    // Al token^ diciamo: se esiste un'istanza della classe, usa quella:
    useExisting: ForbiddenDirective
  }],
  selector: '[appForbidden]'
})
export class ForbiddenDirective implements Validator {
  @Input('appForbidden') forbiddenString!: string[];

  public validate(control: AbstractControl): ValidationErrors | null {
      // const fnInterna = forbiddenValidator('ciaone', 'bella');
      // return fnInterna(control);

      // ^Questa espressione si può semplificare:
    return forbiddenValidator(...this.forbiddenString)(control);
  }

  constructor() { }

}
