import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';
import { pswForbiddenValidator } from '../validators/psw.validators';
import { forbiddenValidator } from '../validators/string.validators';
import { ForbiddenDirective } from './forbidden.directive';

@Directive({
  providers: [{
    multi: true,
    provide: NG_VALIDATORS,
    // Se esiste un'istanza della classe, usa quella
    useExisting: ForbiddenPasswordDirective
  }],
  selector: '[appForbiddenPassword]'
})
export class ForbiddenPasswordDirective {
  @Input('appForbiddenPassword') forbiddenPassword!: string[];

  public validate (control: AbstractControl): ValidationErrors | null {
    return forbiddenValidator(...this.forbiddenPassword)(control);
  }

  constructor() { }

}
