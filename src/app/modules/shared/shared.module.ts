import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForbiddenDirective } from './directives/forbidden.directive';
import { ForbiddenPasswordDirective } from './directives/forbidden-password.directive';



@NgModule({
  declarations: [
    ForbiddenDirective,
    ForbiddenPasswordDirective
  ],
  exports: [
    ForbiddenDirective,
    ForbiddenPasswordDirective
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
