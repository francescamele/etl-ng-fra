import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [{
  component: HomeComponent,
  path: ''
  // path che sarà relativo al percorso base che definisci in ApproutingModule, 
  // da cui deriverà il caricamento delle fatture
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InvoicesRoutingModule { }
