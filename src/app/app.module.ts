// Modules / Core
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

// Routing:
import { AppRoutingModule } from './app-routing.module';

// Components:
import { AppComponent } from './app.component';
import { ArenaComponent } from './components/arena/arena.component';
import { Arena2Component } from './components/arena2/arena2.component';
import { HeroComponent } from './components/arena/hero/hero.component';
import { ButtonsComponent } from './components/buttons/buttons.component';
import { CounterComponent } from './components/counter/counter.component';
import { Hero2Component } from './components/hero2/hero2.component';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

// Directives:
import { HighlightDirective } from './directives/highlight.directive';

import { FarfallesePipe } from './pipes/farfallese.pipe';
import { PluralPipe } from './pipes/plural.pipe';
import { UsersModule } from './modules/users/users.module';
import { SharedModule } from './modules/shared/shared.module';

@NgModule({
  // Dove si declinano comps, directives & pipes: 
  declarations: [
    ArenaComponent,
    Arena2Component,
    AppComponent,
    ButtonsComponent,
    CounterComponent,
    HeroComponent,
    Hero2Component,
    HighlightDirective,
    HomeComponent,
    NotFoundComponent,
    PluralPipe,
    FarfallesePipe
  ],
  // Moduli da cui attingere funzionalità: 
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    SharedModule,
    UsersModule,
    AppRoutingModule
  ],
  // Usually Classi, chiamate 'servizi', che verranno iniettati:
  providers: [
    // Prima di ES6:
    // UserService

    // Equivalente di UserService:
    // {
    //   provide: UserService, // Token
    //   useClass: UserService // Dipendenza
    // }
  ],
  // Components to load all'avvio dell'applicazione: 
  bootstrap: [AppComponent]
})
export class AppModule { }
