import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'farfallese'
})
export class FarfallesePipe implements PipeTransform {

  transform(value: string, ...args: unknown[]): unknown {
    const farfRegex = /[aeiou]+/;
    
    return value.replace(farfRegex, match => `${match}f${match}`);
    // if ( farfRegex.test(value) ) {
    // }

    return null;
  }

}
