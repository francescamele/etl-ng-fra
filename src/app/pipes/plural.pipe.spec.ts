import { transformAll } from '@angular/compiler/src/render3/r3_ast';
import { PluralPipe } from './plural.pipe';

describe('PluralPipe', () => {
  it('create an instance', () => {
    const pipe = new PluralPipe();
    expect(pipe).toBeTruthy();
  });

  it('add "s" to common nouns', () => {
    const pipe = new PluralPipe();
    expect(pipe.transform('car')).toEqual('cars');
    expect(pipe.transform('dog')).toEqual('dogs');
    expect(pipe.transform('teacher')).toEqual('teachers');
    expect(pipe.transform('mountain')).toEqual('mountains');
  });

  it('add "es" to x|s|ss|ch|sh|z', () => {
    const pipe = new PluralPipe();
    expect(pipe.transform('boss')).toEqual('bosses');
    expect(pipe.transform('bus')).toEqual('buses');
    expect(pipe.transform('marsh')).toEqual('marshes');
    expect(pipe.transform('tax')).toEqual('taxes');
    expect(pipe.transform('blitz')).toEqual('blitzes');
    expect(pipe.transform('match')).toEqual('matches');
  });

  it('transform f & fe in ves', () => {
    const pipe = new PluralPipe();
    expect(pipe.transform('leaf')).toEqual('leaves');
    expect(pipe.transform('thief')).toEqual('thieves');
    expect(pipe.transform('wife')).toEqual('wives');
    expect(pipe.transform('feeling')).toEqual('feelings');
  });
});
