import { FarfallesePipe } from './farfallese.pipe';

describe('FarfallesePipe', () => {
  it('create an instance', () => {
    const pipe = new FarfallesePipe();
    expect(pipe).toBeTruthy();
  });

  it('transform la into lafa', () => {
    const pipe = new FarfallesePipe();
    expect(pipe.transform('la')).toEqual('lafa');
    expect(pipe.transform('le')).toEqual('lefe');
    expect(pipe.transform('lala')).toEqual('lafalafa');
  });
});
