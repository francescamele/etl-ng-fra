import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'plural'
})
export class PluralPipe implements PipeTransform {

  /**
   * 
   * @param value data to transform
   * @param args optional arguments to apply
   * @returns 
   */
  transform(value: string, ...args: unknown[]): unknown {
    if (value === 'they') {
      return 'them';
    }
    
    const vesRegex = /(f|fe)$/;
    
    if ( vesRegex.test(value) ) {
      return value.replace(vesRegex, 'ves');
    }
    
    const esRegex = /(s|ss|sh|ch|x|z)$/;
    
    if ( esRegex.test(value) ) {
      return value.concat('es');
    }

    return value.concat('s');
  }

}
