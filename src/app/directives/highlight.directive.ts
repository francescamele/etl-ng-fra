import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  private _starting!: string;

  @Input('appHighlight') public highlightedColour!: string;
  
  constructor(private _el: ElementRef<HTMLElement>) {
    console.log("highlight directive");
  }
  
  @HostListener('mouseover')
  public handleMouseOver(): void {
    console.log('mouse over!');
    this._starting = this._el.nativeElement.style.backgroundColor;
    this._el.nativeElement.style.backgroundColor = this.highlightedColour;
  }

  @HostListener('mouseleave')
  public handleMouseLeave(): void {
    console.log('mouse has left');
    this._el.nativeElement.style.backgroundColor = this._starting;
  }
}
