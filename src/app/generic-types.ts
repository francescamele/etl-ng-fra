/**
 * 31.01 REC I 1h25
 */

function fn(p: string): string {
    return p;
}

function fn2(p: any): any {
    return p;
}


// Se p è tipo any, facendo questo dovrebbe darmi tutti i metodi 
// e proprietà:
let n2 = fn2(5);
n2.toFixed();

function fn3<T>(p: T): T {
    return p;
}

let n3 = fn3(5);
// n3.

function fn4<T, K>(p: T, s: K): T | K {
    return p;
}

let n4 = fn4(5, '6');
// 'n.' non ti dà tanti consigli perché cerca quello in comune tra 
// stringhe e numeri

// function fn6<T>(p: T, s: T): T {
//     return (p, s);
// }

// let n6 = fn6(5, 6);

function fn5(): any {

}

let v = fn5();

// Puoi anche scrivere 'typeof(v)':
if (typeof v === 'number') {
    // Qui 'v.' darà suggerimenti per numeri
}

// Qui, 'v.' non li darà. Se scrivo comunque 'v.length' o ti dà 
// errore o non andrà.

