import { Component, OnInit } from '@angular/core';
import { BigHero } from 'src/app/models/hero2';

@Component({
  selector: 'app-arena2',
  templateUrl: './arena2.component.html',
  styleUrls: ['./arena2.component.scss']
})
export class Arena2Component implements OnInit {
  public hero1: BigHero = new BigHero(
    'Daredevil',
    'https://scontent.ccdn.cloud/image/nospoiler/6a34429b-407f-4cf5-a22b-493282678186/daredevil-mcu-1440x1080.jpg',
    150,
    60
  )
  public hero2: BigHero = new BigHero(
    'Mefisto',
    'https://n7m3z4b2.stackpathcdn.com/wp-content/uploads/2016/06/mefisto-copertina.jpg',
    200,
    40
  )

  constructor() { }

  public handleAttack(character: BigHero) {
    if (character === this.hero1) {
      this.hero2.lifepoints -= this.hero1.damage;
    } else {
      this.hero1.lifepoints -= this.hero2.damage;
    }
    if (this.hero1.lifepoints <= 0) {
      this.hero1.lifepoints = 0;
    }
    if (this.hero2.lifepoints <= 0) {
      this.hero2.lifepoints = 0;
    }
  }

  ngOnInit(): void {
  }

}
