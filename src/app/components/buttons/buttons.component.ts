import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buttons',
  templateUrl: './buttons.component.html',
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent implements OnInit {
  public codeColor: string = '';
  public codeMessage: string = 'What is the code colour?';

  constructor() { }

  ngOnInit(): void {
  }

  public changeCodeColor(ev: string) {
    this.codeColor = ev;
    this.codeMessage = 'The code is ' + ev + '.';
  }

  // public codeColor: boolean;
  // public codeMessage: string = 'What is the code colour?';
  // public button: string = document.getElementsByClassName('white');

  // constructor() { }

  // ngOnInit(): void {
  // }

  // public changeCodeColor(ev: boolean) {
    
  // }

}
