import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BigHero } from 'src/app/models/hero2';

@Component({
  selector: 'app-hero2',
  templateUrl: './hero2.component.html',
  styleUrls: ['./hero2.component.scss']
})
export class Hero2Component implements OnInit {
  @Input() public character!: BigHero;
  @Output() public attack: EventEmitter<BigHero> = new EventEmitter;

  constructor() { }

  public handleClick(): void {
    this.attack.emit(this.character);
  }

  ngOnInit(): void {
  }

}
