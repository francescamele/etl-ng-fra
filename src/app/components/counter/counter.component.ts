import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-counter', // <app-counter></app-counter>
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {
  @Input() public i: number = 0;
  // public button = document.getElementsByName('button');

  @Output() public limitReached: EventEmitter<number> = new EventEmitter;
  
  constructor() { }

  ngOnInit(): void {
  }

  public changeNum(isSum: any, e: MouseEvent): void {
    // void = non restituisce un valore
    // DEVI scrivere : void perché così sia tu che chiunque altro 
    // legga sa che non può mettere un return.
    // if (isSum) {
    //   this.i++;
    //   // Metto questo return invece dell'else perché così non 
    //   // aggiungo un altro livello sullo stack.
    //   return;
    // }

    // this.i--;
    if (isSum) {
      this.i++;
      // Metto questo return invece dell'else perché così non 
      // aggiungo un altro livello sullo stack.
    } else {
      this.i--;
    }

    if (this.i >= 3 || this.i <= -3){
      this.limitReached.emit(this.i);
    }
  }

  // public subtract() {
  //   this.i--;
  // }

  // public add() {
  //   this.i++;
  // }

}
