import { Component, OnInit } from '@angular/core';
import { Hero } from 'src/app/models/hero';

@Component({
  selector: 'app-arena',
  templateUrl: './arena.component.html',
  styleUrls: ['./arena.component.scss']
})
export class ArenaComponent implements OnInit {
  public hero1: Hero = new Hero (
    'Vegeta',
    'https://www.hallofseries.com/wp-content/uploads/2016/03/image-85.jpg',
    350,
    80
    );
  public hero2: Hero = new Hero (
    'Cicciobello',
    'https://m.media-amazon.com/images/I/810WwTpqZ9L._AC_SY550_.jpg',
    500,
    45
    );

  constructor(
  ) { }

  public handleAttack(hero: Hero): void {
    if (this.hero1 === hero) {
      this.hero2.lifepoints -= this.hero1.damage;
    } else {
      this.hero1.lifepoints -= this.hero2.damage;
    }
  }

  ngOnInit(): void {
  }

}
