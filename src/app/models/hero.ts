export class Hero {
    constructor (
        public name: string,
        public image: string,
        public lifepoints: number,
        public damage: number
    ) {
        //
    }
}