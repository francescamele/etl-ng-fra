import { Component } from '@angular/core';
// import { setTimeout } from 'timers';

@Component({
  selector: 'app-root', // <app-root>
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title: string = 'etl-ng-fra';
  public startingNumber: number = 3;

  constructor() {
    setTimeout(() => this.startingNumber = 12, 3000)
  }

  public handleLimitReached(payload: number): void {
    alert(payload);
  }
  // public perché quello che invoco nei template 
  // dev'essere public
  // public handleClick() {
  //   alert('Hai fatto click');
  // }
  
}
