import { TestBed } from '@angular/core/testing';

import { ButtaloService } from './buttalo.service';

describe('ButtaloService', () => {
  let service: ButtaloService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ButtaloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
